from flask import Flask, render_template
import asyncio
import json
import logging
import websockets


logging.basicConfig()


STATE = {'value': 0}

USERS = set()


def state_event():
    return json.dumps({'type': 'state', **STATE})


def users_event():
    return json.dumps({'type': 'users', 'count': len(USERS)})


async def notify_state():
    if USERS:
        message = state_event()
        await asyncio.wait([user.send(message) for user in USERS])


async def notify_users():
    if USERS:
        message = users_event()
        await asyncio.wait([user.send(message) for user in USERS])


async def register(ws):
    USERS.add(ws)
    await notify_users()


async def unregister(ws):
    USERS.remove(ws)
    await notify_users()


async def counter(ws, path):
    await register(ws)
    try:
        await ws.send(state_event())
        async for message in ws:
            data = json.loads(message)
            if data['action'] == 'minus':
                STATE['value'] -= 1
                await notify_state()
            elif data['action'] == 'plus':
                STATE['value'] += 1
                await notify_state()
            else:
                logging.error(' '.join(['unsupported_event:', data]))
    finally:
        await unregister(ws)


def init_app():
    app = Flask(__name__)

    @app.route('/', methods=['GET'])
    def connect_user():
        return render_template('content.html', title='Flask app')

    app.run(host='localhost', port=8050)


if __name__ == '__main__':
    start_server = websockets.serve(counter, 'localhost', 8080)
    asyncio.get_event_loop().run_until_complete(start_server)
    asyncio.get_event_loop().run_in_executor(None, init_app)
    asyncio.get_event_loop().run_forever()
