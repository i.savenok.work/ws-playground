import asyncio
from typing import Optional

import requests
import time


class TimeDelay:

    def __init__(self): self.start = time.time()

    @property
    def value(self): return time.time() - self.start


def fetch_events(page_num: int) -> Optional[dict]:
    try:
        # print(f'get https://api.github.com/events?page={page_num}...')
        response = requests.get(f'https://api.github.com/events?page={page_num}')
        # print(f'get https://api.github.com/events?page={page_num}: {response.status_code}')
        if response.ok:
            return response.json()
    except Exception as e:  # noqa
        return


async def get_all_users_events_async(pages_num) -> list:
    loop = asyncio.get_event_loop()
    futures = [
        loop.run_in_executor(None, fetch_events, page_num)
        for page_num in range(pages_num)
    ]
    results = [await f for f in futures]
    print(', '.join([str(type(r)) for r in results]))
    return results


def get_all_users_events(pages_nums: int, asynchronously: bool) -> list:
    if asynchronously:
        loop = asyncio.get_event_loop()
        r = loop.run_until_complete(get_all_users_events_async(pages_nums))
        return r
    else:
        results = []
        for page_num in range(pages_nums):
            results.append(fetch_events(page_num))
    return results


if __name__ == '__main__':
    delay, pages_num = TimeDelay(), 50
    get_all_users_events(pages_nums=pages_num, asynchronously=False)
    print(f'Not async: {delay.value}')
    delay = TimeDelay()
    get_all_users_events(pages_nums=pages_num, asynchronously=True)
    print(f'Async: {delay.value}')
