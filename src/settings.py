import os


APP_HOST = os.getenv('APP_HOST', 'localhost')
APP_PORT = os.getenv('APP_PORT', 8000)

WS_SERVER_HOST = os.getenv('WS_SERVER_HOST', 'localhost')
WS_SERVER_PORT = os.getenv('WS_SERVER_PORT', 8080)

WS_SERVER_PUBLIC_ADDRESS = os.getenv('WS_SERVER_PUBLIC_HOST', f'ws://{WS_SERVER_HOST}:{WS_SERVER_PORT}/')
