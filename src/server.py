import json
import logging
import random
from typing import Optional, List

import websockets
import asyncio
from flask import Flask, render_template
import settings


class NamesGenerator:
    F = ['жоский', 'забивной', 'ебанутый', 'кричащий', 'охуительно тупой', 'деревянный', 'супер', 'украинский']
    S = ['трап', 'чеченец', 'пролетарий', 'el Primero', 'курощуп', 'ногоед', 'славянин', 'нацист', 'школьник']

    @classmethod
    def generate(
            cls): return f'{cls.F[random.randrange(0, len(cls.F) - 1)]} {cls.S[random.randrange(0, len(cls.S) - 1)]}'


class User:
    name: str

    def __init__(self, ws):
        self.ws = ws
        self.name = NamesGenerator.generate()


class ConnectedUsers(dict):

    async def register(self, ws) -> User:
        user = User(ws)
        self[id(ws)] = user
        await CHAT_BUFFER.add_message(F'{user.name} connected.')
        return user

    async def unregister(self, ws) -> Optional[User]:
        user = self.pop(id(ws))
        if user:
            await CHAT_BUFFER.add_message(f'{user.name} left.')
        return user


CONNECTED_USERS = ConnectedUsers()


class ChatBuffer(list):

    async def add_message(self, msg: str, user: User = None):
        if not msg:
            return

        text = msg
        if user:
            text = f'{user.name}: {text}'
        self.append(text)

        if len(self) > 50:
            self.pop()

        await Event.new_message(text)


CHAT_BUFFER = ChatBuffer()


class Event:
    NEW_MESSAGE = 'new_message'

    @classmethod
    async def new_message(cls, text: str): await cls.send({'type': cls.NEW_MESSAGE, 'data': {'text': text}})

    @classmethod
    async def send(cls, msg: dict, users: List[User] = None):
        users_to_send = users or CONNECTED_USERS
        if not users_to_send:
            return
        msg = json.dumps(msg)
        await asyncio.wait([user.ws.send(msg) for user in users_to_send.values()])


class InputMessage:
    NEW_CHAT_MESSAGE = 'new_chat_msg'
    VALID_TYPES = [NEW_CHAT_MESSAGE]

    data: dict
    type: str

    def __new__(cls, raw: str, *args, **kwargs) -> Optional['InputMessage']:
        try:
            data = json.loads(raw)
            event_type = data['type']
            assert event_type in cls.VALID_TYPES, f'Unknown message type: {event_type}'

            obj = super(InputMessage, cls).__new__(cls)
            obj.type = event_type
            obj.data = data['data']
            return obj

        except Exception as e:
            logging.error(f'Invalid ws message: {raw}, error: {e}')


async def handle_ws(ws, path):
    user = await CONNECTED_USERS.register(ws)
    try:
        async for message in ws:
            message = InputMessage(message)
            if message:
                if message.type == InputMessage.NEW_CHAT_MESSAGE:
                    await CHAT_BUFFER.add_message(
                        msg=message.data['text'],
                        user=user
                    )
    finally:
        await CONNECTED_USERS.unregister(ws)


def init_app():
    app = Flask(__name__)

    @app.route('/', methods=['GET'])
    def connect_user():
        return render_template(
            'content.html',
            title='Nice page, awesome content',
            ws_address=settings.WS_SERVER_PUBLIC_ADDRESS,
            chat_log=CHAT_BUFFER,
        )

    app.run(host=settings.APP_HOST, port=settings.APP_PORT)


if __name__ == '__main__':
    init_ws_server = websockets.serve(handle_ws, settings.WS_SERVER_HOST, settings.WS_SERVER_PORT)
    asyncio.get_event_loop().run_until_complete(init_ws_server)
    asyncio.get_event_loop().run_in_executor(executor=None, func=init_app)
    asyncio.get_event_loop().run_forever()
